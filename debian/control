Source: voacapl
Section: hamradio
Priority: optional
Maintainer: David da Silva Polverari <polverari@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13), gfortran
Standards-Version: 4.7.0
Homepage: https://www.qsl.net/hz1jw/voacapl/index.html
Vcs-Browser: https://salsa.debian.org/debian/voacapl
Vcs-Git: https://salsa.debian.org/debian/voacapl.git

Package: voacapl
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, voacapl-data
Suggests: pythonprop
Description: HF propagation prediction tool
 voacapl is a program that uses empirical data to predict point-to-point path
 loss and coverage of a radio transceiver operating in the high-frequency (3 to
 30 Mhz) range, if given as inputs data such as the transmitting and receiving
 antennas, solar weather, and time/date.
 .
 It is a Linux port of VOACAP (Voice of America Coverage Analysis Program), a
 modified version of IONCAP (Ionospheric Communication Analysis and Prediction
 Program), originally developed by the National Telecommunications and
 Information Administration (NTIA).
 .
 It provides a complex, albeit powerful, command line interface, used by for
 both amateur ("ham radio") and professional radio operators alike. The
 suggested pythonprop package provides an easier graphical interface for
 voacapl, accepting inputs as fields and plotting the results as graphics.

Package: voacapl-data
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: voacapl
Description: HF propagation prediction tool (data files)
 voacapl is a program that uses empirical data to predict point-to-point path
 loss and coverage of a radio transceiver operating in the high-frequency (3 to
 30 Mhz) range, if given as inputs data such as the transmitting and receiving
 antennas, solar weather, and time/date.
 .
 It is a Linux port of VOACAP (Voice of America Coverage Analysis Program), a
 modified version of IONCAP (Ionospheric Communication Analysis and Prediction
 Program), originally developed by the National Telecommunications and
 Information Administration (NTIA).
 .
 It provides a complex, albeit powerful, command line interface, used by for
 both amateur ("ham radio") and professional radio operators alike. The
 suggested pythonprop package provides an easier graphical interface for
 voacapl, accepting inputs as fields and plotting the results as graphics.
 .
 This package contains the data files needed by voacapl.
